# URL shortener for AppLift
build:

`docker build -t app .`

run app with:

`docker run -p 3000:3000 app`

example request:

`curl http://localhost:3000/links -H "Content-Type: application/json" -d '{"address": "example.com"}' -X POST`
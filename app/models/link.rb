class Link < ApplicationRecord
  before_create :generate_slug

  def generate_slug
    new_slug = nil
    loop do
      new_slug = SecureRandom.urlsafe_base64(10)
      break unless Link.where(slug: new_slug).exists?
    end
    self.slug = new_slug
  end

  def short_url
    Rails.application.routes.url_helpers.link_url(
      host: 'localhost:3000',
      slug: slug
    )
  end
end

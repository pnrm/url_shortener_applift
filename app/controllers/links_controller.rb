class LinksController < ApplicationController
  include ApplicationHelper
  skip_before_action :verify_authenticity_token, only: [:create]

  def create
    link = Link.create(address: params[:address])
    render json: {
      short_url: link.short_url,
      redirect_to: link.address
    }
  end

  def show
    link = Link.find_by(slug: params[:slug])
    redirect_to url_with_protocol(link.address)
  end
end

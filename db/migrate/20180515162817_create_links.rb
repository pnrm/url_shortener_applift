class CreateLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :links do |t|
      t.string :slug
      t.string :address
      t.timestamps
    end
  end
end

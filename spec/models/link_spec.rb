require 'rails_helper'

RSpec.describe Link, type: :model do
  it 'has unique slug' do
    links = [
      Link.create(address: 'example.com'),
      Link.create(address: 'example.com'),
      Link.create(address: 'example.com'),
      Link.create(address: 'example.com'),
      Link.create(address: 'example.com')
    ]
    expect(links.pluck(:slug).uniq.count).to eq(links.count)

  end

  it 'generates slug when creating' do
    link = Link.create(address: 'example.com')
    expect(link.slug).not_to be_empty
  end
end
